安装CP2K程序对新手来说是一件略有挑战性的事。虽然GitHub上有预编译版下载即用，但其计算效率远不如在自己机器上编译的，因此自行编译仍较有必要。网上已有不少详细教程，例如本公众号过去半年左右就推出过2篇

[从零开始安装CP2K 8.1 (patched with PLUMED)](https://mp.weixin.qq.com/s/l5rdV2epVu8tXF6wYSMhUA)  
[离线安装CP2K-2022.2](https://mp.weixin.qq.com/s/huV5p01e622_EqQcVXFTRQ)

但上述两篇是使用GCC编译的，要求gcc版本较高，通常使用gcc-9.3.0，另外还需安装OpenBLAS库。因此有必要出一篇Intel编译器编译CP2K的离线安装教程，对gcc版本几乎无要求，也无需安装OpenBLAS。笔者机子上Intel编译器是Intel Parallel Studio XE 2019，含Intel MKL。gcc为4.8.5，cmake为3.22.1。openmpi使用4.1.1版，采用Intel编译器编译，此版openmpi亦可用于量化软件ORCA的使用，一举两得。

# 0. 安装openmpi
出于完整性考虑，此处展示如何使用Intel编译器编译openmpi，已安装的读者可跳过此小节。到官网
```
https://www.open-mpi.org/software/ompi/v4.1
```
下载压缩包，笔者下载的压缩包为openmpi-4.1.1.tar.bz2。解压，编译
```
tar -jxf openmpi-4.1.1.tar.bz2
cd openmpi-4.1.1
./configure --prefix=$HOME/software/openmpi-4.1.1 FC=ifort CC=icc CXX=icpc

make -j24
make install
```
此处使用24核并行编译。完成后在`~/.bashrc`中写上环境变量
```
export PATH=$HOME/software/openmpi-4.1.1/bin:$PATH
export LD_LIBRARY_PATH=$HOME/software/openmpi-4.1.1/lib:$LD_LIBRARY_PATH
export CPATH=$HOME/software/openmpi-4.1.1/include:$CPATH
```
执行`source ~/.bashrc`或退出重登，以使环境变量生效。

# 1. 下载
到以下网址下载压缩包
```
https://www.cp2k.org/static/downloads
```
笔者下载的压缩包名称分别为
```
elpa-2021.11.002.tar.gz
gsl-2.7.tar.gz
libxc-5.2.3.tar.gz
libxsmm-1.17.tar.gz
plumed-src-2.8.0.tgz
spglib-1.16.2.tar.gz
```
到GitHub下载CP2K
```
https://github.com/cp2k/cp2k/releases
```
压缩包名称为`cp2k-2022.2.tar.bz2`。当然也可以到这些库的官网去下载相同名称的压缩包。注意CP2K-2022.2内置了这些库的版本号，如果读者下载其他版本压缩包，则编译CP2K时无法识别压缩包名称，编译工具链脚本会试图联网下载符合要求的库，那就达不到本文离线安装的目的了。原则上这些库可以逐一手动编译，这里我们就不费这功夫了，把大部分库留到最后让CP2K的工具链自行编译。若不需要plumed库，则无需下载plumed和gsl。在编译CP2K过程中，LibXC和Libint库是经常碰到报错的环节，因此我们单独编译这两个库，以便遇到报错时方便解决。

# 2. 编译LibXC
依次运行
```
tar -zxf libxc-5.2.3.tar.gz
cd libxc-5.2.3
mkdir build && cd build

cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/software/libxc_5_2_3 \
-DBUILD_SHARED_LIBS=NO -DCMAKE_C_COMPILER=icc -DCMAKE_Fortran_COMPILER=ifort \
-DCMAKE_INSTALL_LIBDIR=lib -DENABLE_FORTRAN=ON -DENABLE_PYTHON=OFF -DDISABLE_LXC=ON \
-DBUILD_TESTING=NO ..

make -j24
make install
```

成功后，在`~/.bashrc`文件里写上环境变量
```
# LibXC
export LD_LIBRARY_PATH=$HOME/software/libxc_5_2_3/lib:$LD_LIBRARY_PATH
export CPATH=$HOME/software/libxc_5_2_3/include:$CPATH
```
执行`source ~/.bashrc`或退出重登，以使环境变量生效。压缩包和文件夹libxc-5.2.3无用，可以删掉。

若有读者按本公众号之前的教程安装过OpenMolcas（见文末链接），那么可省去上述编译过程，直接利用编译OpenMolcas时用到的LibXC，在`~/.bashrc`文件里写上
```
export CPATH=$MOLCAS/build/External/Libxc_install/include:$CPATH
export LD_LIBRARY_PATH=$MOLCAS/build/External/Libxc_install/lib:$LD_LIBRARY_PATH
```
即可。

# 3. 编译Libint
到以下网址下载CP2K特供版Libint
```
https://github.com/cp2k/libint-cp2k/releases
```
笔者下载的是libint-v2.7.0-beta.5-cp2k-lmax-4.tgz，对于一般的第一性原理计算就够用了。最高角动量数字4/5/6越大，编译时间越长。解压，进入目录
```
tar -zxf libint-v2.7.0-beta.5-cp2k-lmax-4.tgz
cd libint-v2.7.0-beta.5-cp2k-lmax-4/
```

开始编译，依次运行
```
CC=icc CXX=icpc FC=ifort cmake . -DCMAKE_INSTALL_PREFIX=$HOME/software/libint_2_7_0 \
 -DLIBINT2_INSTALL_LIBDIR=$HOME/software/libint_2_7_0/lib \
 -DENABLE_FORTRAN=ON -DREQUIRE_CXX_API=OFF

cmake --build . -j 24
cmake --build . --target install
```
此处笔者使用24核并行编译，读者请根据自己机子实际情况修改。完成后在`~/.bashrc`文件里写上
```
# LIBINT
export CPATH=$HOME/software/libint_2_7_0/include:$CPATH
export LD_LIBRARY_PATH=$HOME/software/libint_2_7_0/lib:$LD_LIBRARY_PATH
```
执行`source ~/.bashrc`或退出重登，以使环境变量生效。同理可删除libint压缩包和解压文件夹，只需保留目录libint_2_7_0。

# 4. 编译CP2K
到放置cp2k压缩包的目录，依次运行
```
tar -jxf cp2k-2022.2.tar.bz2
cd cp2k-2022.2/tools/toolchain/scripts/
rm -f get_openblas_arch.sh
touch get_openblas_arch.sh
chmod +x get_openblas_arch.sh
cd ..
mkdir build && cd build
```
这里我们清空了get_openblas_arch.sh文件内容，让它不要下载OpenBLAS。然后将下载好的5个压缩包挪到`build/`目录下，依次运行
```
cd ..

./install_cp2k_toolchain.sh --with-gcc=no --with-intel=system --with-cmake=system \
--with-openmpi=system --with-openblas=no --with-mkl=system --with-scalapack=system \
--with-libint=system --with-fftw=system --with-libxc=system --with-plumed \
--with-sirius=no --with-cosma=no --with-libvori=no -j 24
```

此处亦使用24核并行编译。写`=system`的表示使用系统上已编译好的库。fftw在Intel MKL里有，无需自行安装。注意仔细观察屏幕上各个库的识别情况。若无报错，则接着运行
```
cp install/arch/* ../../arch/
source install/setup
cd ../..
make -j24 ARCH=local VERSION="ssmp psmp" >make.log 2>&1
```
此处我们将编译过程输出到make.log文件中，万一编译报错，可查看该文件中的报错信息。成功后可在`cp2k-2022.2/exe/local/`目录下找到一堆可执行文件，例如`cp2k.ssmp`，`cp2k.psmp`等等。在`~/.bashrc`文件中写入环境变量
```
# CP2K
source $HOME/software/cp2k-2022.2/tools/toolchain/install/setup
export PATH=$HOME/software/cp2k-2022.2/exe/local:$PATH
```
执行`source ~/.bashrc`或退出重登，以使环境变量生效。

# 5. 测试
我们到晶体开放数据库(COD)下载一个实际的金刚石单胞测试一下
```
http://crystallography.net/cod
```
点击左边Search，在text里输入diamond，在chemical formula输入元素C，点击Send，例如下载COD ID为9011575的cif文件。启动Multiwfn，载入9011575.cif文件，输入cp2k 4个字母进入CP2K输入文件产生功能，指定文件名为diamond.inp，按-1再按4把任务类型修改为变胞优化，最后按数字0即可产生CP2K输入文件。原则上应测试各项参数的收敛性，不过我们此处是程序正常性测试，就不讲究了。可打开inp文件检查一下结构合理性，然后提交CP2K任务
```
mpirun -np 24 cp2k.popt diamond.inp >diamond.out 2>&1 &
```
即使用24个MPI进程并行计算。体系简单，优化四、五步就正常结束了。为使用方便，可在`~/.bashrc`中定义别名
```
alias cp2k='mpirun -np 24 cp2k.popt'
```
以后简单运行
```
cp2k diamond.inp >diamond.out 2>&1 &
```
即可。但要注意alias别名无法在Shell脚本里被识别，若使用脚本提交CP2K计算任务（尤其是在集群上），记得同时在脚本里也写上该别名的定义。

# 6. 可能遇到的问题
以下报错信息为笔者在不同系统、不同环境下尝试时碰到，并非一定会碰到。若读者编译未遇报错，可跳过此小节。

## 6.1 `error: function call is not allowed`
报错信息类似
```
In file included from /home/jxzou/software/libint-v2.7.0-beta.5-cp2k-lmax-4/include/libint2.h(35):
/home/jxzou/software/libint-v2.7.0-beta.5-cp2k-lmax-4/include/libint2/util/generated/libint2_types.h(28): error: function call is not allowed in a constant expression
  # if __has_include(<libint2_types.h>)
```
在使用Intel OneAPI编译libint时可能遇到此报错。解决方法有两种：（1）使用特定版本的Intel OneAPI，笔者测试过2021.2是可以的；（2）安装更高版本的gcc。笔者机器上系统自带gcc为4.8.5，在安装gcc-8.5.0后，仍用Intel OneAPI按此文编译，便无此报错。但第（2）种做法会引出一个问题：既然要安装高版本gcc，不如所有步骤都用gcc编译，不用Intel编译器。这就由读者自行决定了。

## 6.2 `ld: cannot find -lz`
编译CP2K时成功产生`cp2k.ssmp`可执行程序，但未产生`cp2k.psmp`，且编译过程报错`ld: cannot find -lz`，表示找不到libz.so动态库。解决思路：找到libz.so并导出其库路径。以笔者机器情况为例，经查找发现有两处存在libz.so，第一处在`/usr/lib64/`下
```
/usr/lib64/libz.so.1 -> libz.so.1.2.7
/usr/lib64/libz.so.1.2.7
```

第二处在Anaconda Python3的库路径中
```
/opt/anaconda3/lib/libz.so -> libz.so.1.2.11
/opt/anaconda3/lib/libz.so.1 -> libz.so.1.2.11
/opt/anaconda3/lib/libz.so.1.2.11
```

任选一处使用即可。若想采用第一处的libz.so，需创建软链接，例如
```
mkdir -p $HOME/.local/lib
cd $HOME/.local/lib
ln -s /usr/lib64/libz.so.1.2.7 libz.so
```
在执行`make -j24 ARCH=local`这一步之前，打开`arch/local.psmp`文件，到最后两三行的LIBS段落中删掉`-lz`，然后在此段落末尾放一空格，补写上`-L$(HOME)/.local/lib -lz`。

若想采用第二处，则无需创建软链接，直接在`~/.bashrc`里写上
```
export LD_LIBRARY_PATH=/opt/anaconda3/lib:$LD_LIBRARY_PATH
```
执行`source ~/.bashrc`或退出重登，以使环境变量生效。此时无需修改`arch/local.psmp`文件。

上述任一改动完成后，回到`cp2k-2022.2/`目录下，执行
```
make -j24 ARCH=local VERSION="psmp" >make.log 2>&1
```
因为`cp2k.ssmp`可执行程序已产生，可跳过其编译步骤。过几分钟便可正常编译出`cp2k.psmp`。

## 相关阅读
[使用Multiwfn非常便利地创建CP2K程序的输入文件](http://sobereva.com/587)  
[离线安装OpenMolcas-v22.06](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85OpenMolcas-v22.06.md)  
[编译MPI并行版OpenMolcas](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%BC%96%E8%AF%91MPI%E5%B9%B6%E8%A1%8C%E7%89%88OpenMolcas.md)  
[Linux下安装高版本GCC](https://gitlab.com/jxzou/qcinstall/-/blob/main/Linux%E4%B8%8B%E5%AE%89%E8%A3%85%E9%AB%98%E7%89%88%E6%9C%ACGCC.md)  
[Linux下安装Intel oneAPI](https://mp.weixin.qq.com/s/7pQETkrDO1C83vQjKQqI4w)  
[离线安装CP2K-2024.1_Intel编译器版](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85CP2K-2024.1_Intel%E7%BC%96%E8%AF%91%E5%99%A8%E7%89%88.md?ref_type=heads)

