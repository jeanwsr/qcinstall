Block2是Garnet Chan课题组继Block-1.5后的全新改进版，代码重构，功能更多，该版本主要开发者为Huanchen Zhai。程序的说明文档和源代码分别见
```
https://block2.readthedocs.io/en/latest/index.html
https://github.com/block-hczhai/block2-preview
```
这个程序可以单独使用（用户需提供FCIDUMP文件），也可与PySCF联用。后者使用方式与block-1.5无异，因此我们只需编译好block2，便仍可使用PySCF调用其做DMRG计算。block2有OpenMP并行版和MPI并行版两种，用户需选择一种来安装，本文将着重介绍后者。因为以前block-1.5常用MPI并行版，所以将block2编译成MPI并行版的话，二者更像，这样以前写过的输入文件里核数、内存都是通用的，一个字都不用改。


# 1. 联网安装
## 1.1 安装OpenMP并行版
```
pip install block2
```
## 1.2 安装MPI并行版
```
pip install block2-mpi
```
注意安装block2-mpi后，读者还需自行安装openmpi-4.0.x。由于笔者没有联网安装过block2，读者若碰到安装问题，请仔细阅读说明文档或到程序GitHub[issues](https://github.com/block-hczhai/block2-preview/issues)区提问，勿向笔者提问。


# 2. 离线安装MPI版block2
安装该版本需满足这些前提条件：

- MKL数学库
- python3
- gcc-9.2.0
- pybind11 (<=2.10.1)
- mpi4py
- openmpi

如果读者机子上有Intel编译器，则一般内含MKL库。笔者机子上Intel编译器版本是Intel OneAPI 2021.1。如果安装过Anaconda Python3，则有python3。笔者机子上为Python 3.11.7，同时笔者发现3.9.12版本在编译mpi4py时会报错（详见文末）。

对于gcc-9.2.0，如果是在集群上，一般都提供了module工具，运行`module avai`可以查看有哪些编译器或库可供加载，如果有的话，例如运行`module load gcc/9.2.0`即可加载。如果实在没有，可以自行安装，见[Linux下安装高版本GCC](https://mp.weixin.qq.com/s/ljSXoG0F8iLVV3bGTiUgcg)。笔者尝试过用gcc-8.5.0编译block2亦可，但gcc-5.4.0版本会报错，中间版本的gcc读者可自行尝试。

pybind11库，mpi4py库和openmpi这三个需要我们单独安装，注意如果没有gcc和这三个库，应当先安装gcc，然后再安装这三个。以下安装步骤建立在已有MKL、Anaconda Python3和gcc-9.2.0的基础上。

## 2.1 安装pybind11
到[官网](https://github.com/pybind/pybind11/releases)下载pybind11。笔者下载的压缩包是pybind11-2.9.2.tar.gz。解压，进入目录，运行命令

```
tar -zxf pybind11-2.9.2.tar.gz
cd pybind11-2.9.2
python setup.py build
python setup.py install --user
```

在build那一步，屏幕上会出现类似如下提示
```
-- The CXX compiler identification is GNU 9.2.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /public/software/gcc/gcc-9.2/bin/c++ - skipped
```

表明识别出了gcc-9.2，若读者自己编译时发现没有正确识别gcc，应先检查gcc环境变量。`--user`表示安装到个人目录`~/.local/`下，无需写环境变量即可被自动识别。压缩包和刚解压出来的pybind11-2.9.2文件夹没有用处，均可删除。

## 2.2 安装openmpi
此部分为离线安装，openmpi版本要求较为宽松（联网安装才要求openmpi-4.0.x）。此处我们以openmpi-4.1.6为例，因为这个版本也可用于ORCA 6的使用，一举两得。到[官网](https://www.open-mpi.org/software/ompi/v4.0)下载openmpi，笔者下载的压缩包是openmpi-4.1.6.tar.bz2。解压，进入目录，编译

```
tar -jxf openmpi-4.1.6.tar.bz2
mv openmpi-4.1.6 openmpi
cd openmpi

./configure --prefix=$HOME/software/openmpi-4.1.6 \
CC=$HOME/gcc-9.2.0/bin/gcc CXX=$HOME/gcc-9.2.0/bin/g++ \
FC=$HOME/gcc-9.2.0/bin/gfortran

make -j16
make install
```

笔者机子上gcc-9.2.0安装在个人目录下，不在系统路径中，因此此处使用`CC`, `CXX`和`FC`指明了路径。笔者习惯于将程序都装在`$HOME/software/`目录下，读者可按照个人喜好自行修改。压缩包和刚解压出来的openmpi文件夹没有用处，均可删除。此时需在~/.bashrc文件里写上环境变量
```
export PATH=$HOME/software/openmpi-4.1.6/bin:$PATH
export CPATH=$HOME/software/openmpi-4.1.6/include:$CPATH
export LD_LIBRARY_PATH=$HOME/software/openmpi-4.1.6/lib:$LD_LIBRARY_PATH
```

并重登使之生效。此处是用gcc-9.2编译的openmpi，若读者在集群上有相应安装好的openmpi，直接`module load`加载则更方便。

## 2.3 安装mpi4py
到[官网](https://pypi.org/project/mpi4py/#files)下载mpi4py，笔者下载的压缩包是mpi4py-3.1.4.tar.gz。注意，读者需提前检查机子上Python版本，python-3.8/3.11/3.12对应的mpi4py最低要求为3.1.3/3.1.4/3.1.5，勿随意下载一个版本使用。

解压，进入目录，编译
```
tar -zxf mpi4py-3.1.4.tar.gz
cd mpi4py-3.1.4
```

打开mpi.cfg文件，找到Open MPI example，修改openmpi路径为本机真实路径
```
[openmpi]
mpi_dir              = /public/home/jxzou/software/openmpi-4.1.6
```

其中jxzou是笔者的用户名，读者需修改为自己机器上的路径。保存，开始编译，依次运行
```
python setup.py build --mpi=openmpi
python setup.py install --user
```

这样同样会安装到个人目录~/.local/下。压缩包和刚解压出来的mpi4py-3.1.4文件夹没有用处，均可删除。测试是否安装成功，启动python
```python
from mpi4py import MPI
```

没有报错即可。

## 2.4 安装block2
到[官网](https://github.com/block-hczhai/block2-preview/releases)下载block2。笔者下载的压缩包是block2-preview-p0.5.3rc18.zip。解压，进入目录，编译
```
unzip block2-preview-p0.5.3rc18.zip
cd block2-preview-p0.5.3rc18
mkdir build && cd build

CC=$HOME/software/openmpi-4.1.6/bin/mpicc \
CXX=$HOME/software/openmpi-4.1.6/bin/mpic++ \
cmake .. -DUSE_MKL=ON -DBUILD_LIB=ON -DMPI=ON

make -j16
cd ..
mkdir lib
mv build/block2*.so lib/
```

在~/.bashrc里写上环境变量
```
export PYTHONPATH=$HOME/software/block2-preview-p0.5.3rc18:$PYTHONPATH
export PYTHONPATH=$HOME/software/block2-preview-p0.5.3rc18/lib:$PYTHONPATH
```

退出重登，使之生效。在pyblock2/driver/下有个block2main，这是PySCF调用block2的脚本。我们进入PySCF的dmrgscf目录，如果是pyscf-1.7.6，就是
```
pyscf-1.7.6/pyscf/dmrgscf/
```
如果是pyscf-2.x版本，dmrgscf默认不在`pyscf-2.x/`下，需要自行安装，见[离线安装PySCF-2.x-extensions](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85PySCF-2.x-extensions.md)。安装完后，进入目录
```
dmrgscf/pyscf/dmrgscf/
```

复制一份配置文件
```
cp settings.py.example settings.py
```

打开settings.py，修改其中路径为block2main路径，例如笔者机子上是
```
BLOCKEXE = '/home/jxzou/software/block2-preview-p0.5.3rc18/pyblock2/driver/block2main'
BLOCKEXE_COMPRESS_NEVPT = '/home/jxzou/software/block2-preview-p0.5.3rc18/pyblock2/driver/block2main'
```
此处`BLOCKEXE`和`BLOCKEXE_COMPRESS_NEVPT`可以填同一个block2路径，block2会自己识别是否被MPI调用。这点不同于Sandeep Sharma的Block-1.5（其需要填MPI与串行版两个不同路径）。

临时文件路径改为本地某真实路径，例如
```
BLOCKSCRATCHDIR = os.path.join('/scratch/jxzou/pyscf', str(os.getpid()))
```
其余几行`BLOCKSCRATCHDIR`保持注释状态，或直接删除。其他内容不用修改。至此安装结束，以前算过的.py文件拿一个过来测试即可。笔者开发的[MOKIT](https://gitlab.com/jxzou/mokit)开源程序可以自动构造活性轨道，并调用PySCF+Block做大活性空间的DMRG-CASCI, DMRG-CASSCF和DMRG-SC-NEVPT2计算，在CASCI/CASSCF步骤还能给出含有自然轨道的fch文件，例子见[block-1.5的编译和安装](https://gitlab.com/jxzou/qcinstall/-/blob/main/block-1.5%E7%9A%84%E7%BC%96%E8%AF%91%E5%92%8C%E5%AE%89%E8%A3%85.md)第5节。


# 3. 离线安装OpenMP版block2
这个版本的安装比MPI版block2简单，不需要openmpi和mpi4py。能做DMRG-CASCI/CASSCF计算，但不能做DMRG-NEVPT2计算（对不需要此功能、又急着做计算的用户是首选）。
```
unzip block2-preview-p0.5.3rc18.zip
cd block2-preview-p0.5.3rc18
mkdir build && cd build

cmake .. -DUSE_MKL=ON -DBUILD_LIB=ON -DMPI=OFF
make -j16
cd ..
mkdir lib
mv build/block2*.so lib/
```
注意看屏幕信息，是否有正确识别到gcc-9.2。修改settings.py的操作类似。


# 4. 可能遇到的问题
## 4.1
Python 3.9.12或更高版本在安装mpi4py时可能会出现如下类似报错

```
/public/home/jxzou/software/anaconda3/compiler_compat/ld: warning: libopen-rte.so.40, needed by /public/home/jxzou/software/openmpi-4.1.1/lib/libmpi.so, not found (try using -rpath or -rpath-link)
/public/home/jxzou/software/anaconda3/compiler_compat/ld: warning: libopen-orted-mpir.so, needed by /public/home/jxzou/software/openmpi-4.1.1/lib/libmpi.so, not found (try using -rpath or -rpath-link)
/public/home/jxzou/software/anaconda3/compiler_compat/ld: warning: libopen-pal.so.40, needed by /public/home/jxzou/software/openmpi-4.1.1/lib/libmpi.so, not found (try using -rpath or -rpath-link)
/public/home/jxzou/software/anaconda3/compiler_compat/ld: warning: libdl.so.2, needed by /public/home/jxzou/software/openmpi-4.1.1/lib/libmpi.so, not found (try using -rpath or -rpath-link)
/public/home/jxzou/software/anaconda3/compiler_compat/ld: warning: librt.so.1, needed by /public/home/jxzou/software/openmpi-4.1.1/lib/libmpi.so, not found (try using -rpath or -rpath-link)
/public/home/jxzou/software/anaconda3/compiler_compat/ld: warning: libm.so.6, needed by /public/home/jxzou/software/openmpi-4.1.1/lib/libmpi.so, not found (try using -rpath or -rpath-link)
/public/home/jxzou/software/anaconda3/compiler_compat/ld: warning: libutil.so.1, needed by /public/home/jxzou/software/openmpi-4.1.1/lib/libmpi.so, not found (try using -rpath or -rpath-link)
/public/home/jxzou/software/anaconda3/compiler_compat/ld: warning: libz.so.1, needed by /public/home/jxzou/software/openmpi-4.1.1/lib/libmpi.so, not found (try using -rpath or -rpath-link)
...
```

其中可能还会含有`libimf.so`等库的报错，报错信息较长，采用省略号表示。这是由于Anaconda Python的`ld`版本过时引起的，最简单的解决办法是给它改个名字，运行
```
mv /public/home/jxzou/software/anaconda3/compiler_compat/ld /public/home/jxzou/software/anaconda3/compiler_compat/other
```
`ld`的具体路径请读者根据自己机器上的实际情况确定。然后清空`build`目录，重新编译mpi4py
```
rm -rf build/*
python setup.py build --mpi=openmpi
```
编译成功后，可以再把`ld`修改回来
```
mv /public/home/jxzou/software/anaconda3/compiler_compat/other /public/home/jxzou/software/anaconda3/compiler_compat/ld
```

有时在集群上使用的是`module load`加载的Anaconda Python，普通用户没有权限改动其目录下的`ld`。那么读者可以在个人用户目录下安装一个Anaconda Python（各个版本安装包可以在[这里](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85PySCF-2.x.md?ref_type=heads#8%E5%8F%AF%E8%83%BD%E9%81%87%E5%88%B0%E7%9A%84%E9%97%AE%E9%A2%98)找到）。

## 4.2
使用Anaconda3-2022.05（对应Python 3.9.12）在成功安装mpi4py后，执行`from mpi4py import MPI`时可能会出现如下报错
```
undefined symbol: ompi_mpi_logical8
```
怀疑是mpi4py链接到了其他MPI库，但查了一下Anaconda3-2022.05中并没有mpich之类的MPI程序。改用Anaconda3-2022.10（对应Python 3.9.13）后此问题消失。

## 4.3
若在DMRG-NEVPT2计算的输出文件中遇到如下警告信息
```
WARN: DMRG executable file for nevptsolver is the same to the executable file for DMRG solver. If they are both compiled by MPI compilers, they may cause error or random results in DMRG-NEVPT calculation.
```
当您使用的是block2时，可无视此警告，这是正常现象；而若您使用的是block-1.5，这说明`settings.py`文件中两个可执行文件的路径误写成了一样的，应修改为对应的正确路径。

## 4.4
若您编译时始终采用诸如`module load gcc/9.2`形式加载gcc，那后续使用block2做计算时要记得以同样方式加载gcc。若您在集群上使用脚本提交计算任务，应将`module load gcc/9.2`写进提交任务的脚本中。


## 相关阅读
[离线安装PySCF-1.7.6](https://mp.weixin.qq.com/s/xtlOZ8XvEaL4nAiq7W7LeQ)  
[离线安装PySCF-2.x](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85PySCF-2.x.md)  
[离线安装PySCF-2.x-extensions](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85PySCF-2.x-extensions.md)  
[安装基于openmpi的mpi4py](https://mp.weixin.qq.com/s/f5bqgJYG5uAK1Zubngg65g)  
[block-1.5的编译和安装](https://gitlab.com/jxzou/qcinstall/-/blob/main/block-1.5%E7%9A%84%E7%BC%96%E8%AF%91%E5%92%8C%E5%AE%89%E8%A3%85.md)

